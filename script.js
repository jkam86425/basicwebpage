const container = document.querySelector(".container");
const signUpBtn = document.querySelector(".green-bg button");
const logo = document.getElementById("logo");
const submitBtn = document.getElementById("submitButton");
const facebookBtn = document.getElementById("fa-facebook-f");
const instagramBtn = document.getElementById("fa-instagram");
const linkedinBtn = document.getElementById("fa-linkedin-in");

signUpBtn.addEventListener('click', 
() => {
    if (logo.src.match("logo")) {
        console.log("match");
        logo.src = "resources/img/partial.png";
      } else {
        console.log("no match");
        logo.src = "resources/img/logo.png";
      }
    container.classList.toggle("change");
});

submitBtn.addEventListener('click',
() => {
    console.log("submit form")
    var name = document.forms["registrationForm"]["fName"].value;
    console.log(name)
    var email = document.forms["registrationForm"]["fEmail"].value;
    if (name == "" || email == ""){
        alert("All spaces must be filled");
        return false
    }
    window.location.href = "message.html";
});

facebookBtn.addEventListener('click',
() => {
    window.location.href = "message.html";
});

instagramBtn.addEventListener('click',
() => {
    window.location.href = "message.html";
});

linkedinBtn.addEventListener('click',
() => {
    window.location.href = "message.html";
});